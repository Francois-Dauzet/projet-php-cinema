<?php

declare(strict_types=1);

// Test //
///////////////////////////////// function //////////////////////////////////

function nbrePlaceDispo(array $tableau, string $numeroSalle, string $seance){
    return $tableau[$numeroSalle]['places'] = ($tableau[$numeroSalle]['places'] - $tableau[$numeroSalle][$seance]);
}

function prixFilm(array $tableau , string $numeroSalle){
    return $tableau[$numeroSalle]['prix'];
}

function nbrePlacesVendues(array $tableau, string $numeroSalle){
    return ($tableau[$numeroSalle]['seance14h'] + $tableau[$numeroSalle]['seance17h'] + $tableau[$numeroSalle]['seance20h'] + $tableau[$numeroSalle]['seance23h']);
}

function meilleureSeance(array $tableau, string $numeroSalle){
    (int) $topPlaceSeance = -1;
    (string) $topNomSeance = '';
    if ($topPlaceSeance < $tableau[$numeroSalle]['seance14h']){
        $topPlaceSeance = $tableau[$numeroSalle]['seance14h'];
        $topNomSeance = 'Séance de 14h';
    } 
    if ($topPlaceSeance < $tableau[$numeroSalle]['seance17h']){
        $topPlaceSeance = $tableau[$numeroSalle]['seance17h'];
        $topNomSeance = 'Séance de 17h';
    }
    if ($topPlaceSeance < $tableau[$numeroSalle]['seance20h']){
        $topPlaceSeance = $tableau[$numeroSalle]['seance20h'];
        $topNomSeance = 'Séance de 20h';
    }
    if ($topPlaceSeance < $tableau[$numeroSalle]['seance23h']){
        $topPlaceSeance = $tableau[$numeroSalle]['seance23h'];
        $topNomSeance = 'Séance de 23h';
    }
return $topNomSeance;
}

function plusMauvaiseSeance(array $tableau, string $numeroSalle){
    (int) $topPlaceSeance = 99;
    (string) $topNomSeance = '';
    if ($topPlaceSeance > $tableau[$numeroSalle]['seance14h']){
        $topPlaceSeance = $tableau[$numeroSalle]['seance14h'];
        $topNomSeance = 'Séance de 14h';
    } 
    if ($topPlaceSeance > $tableau[$numeroSalle]['seance17h']){
        $topPlaceSeance = $tableau[$numeroSalle]['seance17h'];
        $topNomSeance = 'Séance de 17h';
    }
    if ($topPlaceSeance > $tableau[$numeroSalle]['seance20h']){
        $topPlaceSeance = $tableau[$numeroSalle]['seance20h'];
        $topNomSeance = 'Séance de 20h';
    }
    if ($topPlaceSeance > $tableau[$numeroSalle]['seance23h']){
        $topPlaceSeance = $tableau[$numeroSalle]['seance23h'];
        $topNomSeance = 'Séance de 23h';
    }
return $topNomSeance;
}

function meilleureFilm(array $tableau){
    (int) $topPlaceFilm = -1;
    (string) $topNomFilm = '';
    if ($topPlaceFilm < $tableau['salle1']['seanceTotale']){
        $topPlaceFilm = $tableau['salle1']['seanceTotale'];
        $topNomFilm = $tableau['salle1']['titre'];
    } 
    if ($topPlaceFilm < $tableau['salle2']['seanceTotale']){
        $topPlaceFilm = $tableau['salle2']['seanceTotale'];
        $topNomFilm = $tableau['salle2']['titre'];
    }
    if ($topPlaceFilm < $tableau['salle3']['seanceTotale']){
        $topPlaceFilm = $tableau['salle3']['seanceTotale'];
        $topNomFilm = $tableau['salle3']['titre'];
    }
    if ($topPlaceFilm < $tableau['salle4']['seanceTotale']){
        $topPlaceFilm = $tableau['salle4']['seanceTotale'];
        $topNomFilm = $tableau['salle4']['titre'];
    }
return $topNomFilm;
}

function plusMauvaisFilm(array $tableau){
    (int) $topPlaceFilm = 99;
    (string) $topNomFilm = '';
    if ($topPlaceFilm > $tableau['salle1']['seanceTotale']){
        $topPlaceFilm = $tableau['salle1']['seanceTotale'];
        $topNomFilm = $tableau['salle1']['titre'];
    } 
    if ($topPlaceFilm > $tableau['salle2']['seanceTotale']){
        $topPlaceFilm = $tableau['salle2']['seanceTotale'];
        $topNomFilm = $tableau['salle2']['titre'];
    }
    if ($topPlaceFilm > $tableau['salle3']['seanceTotale']){
        $topPlaceFilm = $tableau['salle3']['seanceTotale'];
        $topNomFilm = $tableau['salle3']['titre'];
    }
    if ($topPlaceFilm > $tableau['salle4']['seanceTotale']){
        $topPlaceFilm = $tableau['salle4']['seanceTotale'];
        $topNomFilm = $tableau['salle4']['titre'];
    }
return $topNomFilm;
}

function entreesSeance(array $tableau, string $numeroSalle, string $seance){
    return $tableau[$numeroSalle][$seance];
} 

function saisieFilm(string $message){
    (string) $film = '';
    while (!$film){
        print($message);
        $film = trim(strval(fgets(STDIN)));
    }
    return $film;
}

function saisiePrix(string $message){
    (float) $prix = 0.0;
    do { print($message);
        $prix = trim(fgets(STDIN));
    } while (!is_numeric($prix));
    return $prix;
}

function saisieChoixFilm(string $message){
    (int) $choix = 0;
    do { print($message);
        $choix = trim(fgets(STDIN));
    } while (!is_numeric($choix) || ($choix < 0 || $choix > 4));
    return $choix;
}

function choixSeance(int $compteur){
    if ($compteur == 0){
        print(PHP_EOL . PHP_EOL . '////// Séance de 14h //////' . PHP_EOL);
        return 'seance14h';
    } else if ($compteur == 1){
        print(PHP_EOL . PHP_EOL . '////// Séance de 17h //////' . PHP_EOL);
        return 'seance17h';
    } else if ($compteur == 2){
        print(PHP_EOL . PHP_EOL . '////// Séance de 20h //////' . PHP_EOL);
        return 'seance20h';
    } else {
        print(PHP_EOL . PHP_EOL . '////// Séance de 23h //////' . PHP_EOL);
        return 'seance23h';
    }
}

/////////////////////////////// tableau de données ///////////////////////////////

(array) $tabSalles = [
    'salle1' => ['places' => 15, 'titre' => '', 'prix' => 0.0, 'seance14h' => 0, 'seance17h' => 0, 'seance20h' => 0, 'seance23h' => 0, 'seanceTotale' => 0],
    'salle2' => ['places' => 10, 'titre' => '', 'prix' => 0.0, 'seance14h' => 0, 'seance17h' => 0, 'seance20h' => 0, 'seance23h' => 0, 'seanceTotale' => 0],
    'salle3' => ['places' => 15, 'titre' => '', 'prix' => 0.0, 'seance14h' => 0, 'seance17h' => 0, 'seance20h' => 0, 'seance23h' => 0, 'seanceTotale' => 0],
    'salle4' => ['places' => 10, 'titre' => '', 'prix' => 0.0, 'seance14h' => 0, 'seance17h' => 0, 'seance20h' => 0, 'seance23h' => 0, 'seanceTotale' => 0]
];


/////////////////////////////// Variables ///////////////////////////////

(string) $seance = '';
(int) $totalPlacesVendu = 0;
(float) $CA = 0.0;



/////////////////////////////// Saisie ///////////////////////////////

print(PHP_EOL . '*** ' . 'Mise en place des films' . ' ***' . PHP_EOL);

// Saisie des 4 films et des prix
for ($i = 1 ; $i < 5 ; $i++){
    printf(PHP_EOL . '/// Salle n° %d ///' . PHP_EOL, $i);
    $tabSalles['salle' . $i]['titre'] = saisieFilm('Film à diffuser ? ');
    $tabSalles['salle' . $i]['prix'] = saisiePrix('Prix de la place ? ');
}


// Boucle de rotation des séances
for ($compteurSeance = 0 ; $compteurSeance < 4 ; $compteurSeance++){

    $seance = choixSeance($compteurSeance);

    (bool) $finSeance = true;

    // Boucle de fin de séance
    while ($finSeance == true){
        (string) $choixFin = "";

        print(PHP_EOL . '*** Séléction des films ***' . PHP_EOL);

        // Affichage des film avec prix et place dispo
        for ($i = 1 ; $i < 5 ; $i++){
            printf('%d: %s // Place libre : %d (%g€)' . PHP_EOL, $i, $tabSalles['salle' . $i]['titre'], $topPlaceSeanceSalle1 = nbrePlaceDispo($tabSalles, 'salle' . $i, $seance), $tabSalles['salle' . $i]['prix']);
        }

        // Saisie du film séléctionner 
        (int) $choixFilmNA = 0;
        do {
            do { $choixFilmNA = saisieChoixFilm('Choisir le film : ');
                if ($tabSalles['salle' . $choixFilmNA][$seance] >= $tabSalles['salle' . $choixFilmNA]['places']){
                    print('Attention nombre maximum de place atteint !!!' . PHP_EOL);
                }
            } while ($tabSalles['salle' . $choixFilmNA][$seance] >= $tabSalles['salle' . $choixFilmNA]['places']);

            // Saisie du nombre de personne(s)
            (int) $nbrePlaceNA = 0;
            $nbrePlaceNA = saisiePrix('Combien de personne(s) : ');
            if ($nbrePlaceNA > ($tabSalles['salle' . $choixFilmNA]['places'] - $tabSalles['salle' . $choixFilmNA][$seance])){
                print('Attention nombre maximum de place atteint !!!' . PHP_EOL);
            }
        } while ($nbrePlaceNA > ($tabSalles['salle' . $choixFilmNA]['places'] - $tabSalles['salle' . $choixFilmNA][$seance]));

        // Ajout des place prise sur le film séléctionner
        $tabSalles['salle' . $choixFilmNA][$seance] = $tabSalles['salle' . $choixFilmNA][$seance] + $nbrePlaceNA;

        // Boucle choix de fin de séance
        while ($choixFin != 'oui' && $choixFin != 'non'){
            print(PHP_EOL . 'Fin de la séance ("oui" ou "non") ? ');
            $choixFin = trim(strval(fgets(STDIN)));
        }
    
        $finSeance = ($choixFin == 'non');
    }
}

/////////////////////////////// Récapitulatif ///////////////////////////////


for ($i = 1 ; $i < 5 ; $i++){
    printf(PHP_EOL . PHP_EOL . PHP_EOL . '*** Salle n° %d ***' . PHP_EOL, $i);
    printf('Le film %s au prix de %g€ la place', $tabSalles['salle' . $i]['titre'], $tabSalles['salle' . $i]['prix']);

    for ($compteurSeance = 0 ; $compteurSeance < 4 ; $compteurSeance++){
        $seance = choixSeance($compteurSeance);
        printf('%d place(s) de vendu (Prix de vente total: %g€)', $tabSalles['salle' . $i][$seance], $tabSalles['salle' . $i][$seance] * prixFilm($tabSalles ,'salle' . $i));
    }
    printf(PHP_EOL . PHP_EOL . PHP_EOL . '*** Récapitulatif pour la salle n° %d ***' . PHP_EOL, $i);
    printf(PHP_EOL . 'Nombres de places totales vendu: %d', nbrePlacesVendues($tabSalles, 'salle' . $i));
    printf(PHP_EOL . 'Prix de vente total: %g€',nbrePlacesVendues($tabSalles, 'salle' . $i) * prixFilm($tabSalles ,'salle' . $i));
    $CA += nbrePlacesVendues($tabSalles, 'salle' . $i) * prixFilm($tabSalles ,'salle' . $i);
    $tabSalles['salle' . $i]['seanceTotale'] += nbrePlacesVendues($tabSalles, 'salle' . $i);
    $totalPlacesVendu += $tabSalles['salle' . $i]['seanceTotale'];
    printf(PHP_EOL . "Top séance : %s", meilleureSeance($tabSalles, 'salle' . $i));
    printf(PHP_EOL . "Bad séance : %s", plusMauvaiseSeance($tabSalles, 'salle' . $i));
}

print(PHP_EOL . PHP_EOL . PHP_EOL . '*** Récapitulatif de la journée ***' . PHP_EOL);
printf(PHP_EOL . 'Nombres de places totales vendu: %d', $totalPlacesVendu);
printf(PHP_EOL . 'Prix de vente total: %g€', $CA);
printf(PHP_EOL . 'Top film : %s', meilleureFilm($tabSalles));
printf(PHP_EOL . 'Bad film : %s', plusMauvaisFilm($tabSalles));

(int) $totalPlacesVendu = 0;